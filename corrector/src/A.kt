fun isHappy(s: String): Boolean {
    for (c in s) {
        if (c == '-')
            return false
    }
    return true
}

fun rotate(s: String, i: Int, k: Int): String {
    var res = String() + s
    for (j in i..(i + k - 1)) {
        res = res.replaceRange(j, j + 1, if (res[j] == '-') "+" else "-")
    }
    return res
}


fun getMinForS(s: String, k: Int): Int? {
    if (k <= 0 || k > s.length)
        return null
    var s = s
    var n = 0

    for (i in 0..(s.length - k)) {
        if (s[i] == '-') {
            n++
            s = rotate(s, i, k)
        }
    }

    return if (isHappy(s)) n else null
}

fun main(args: Array<String>) {
    val t = readLine()!!.toInt()
    for (x in 1..t) {
        val str = readLine()!!.split(" ")
        print("Case #${x}: ${getMinForS(str[0], str[1].toInt())?.toString() ?: "IMPOSSIBLE"}\n")
    }
}
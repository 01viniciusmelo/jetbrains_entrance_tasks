#ifndef COMPLEXNUM_COMPLEXNUM_HPP
#define COMPLEXNUM_COMPLEXNUM_HPP

#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;

class ComplexNum {
private:
	double real, imag;

public:
	ComplexNum();;
	
	ComplexNum(double real);;
	
	ComplexNum(double real, double imag);;
	
	ComplexNum(ComplexNum &cn);;
	
	ComplexNum &operator=(ComplexNum &cn);
	
	ComplexNum &operator+(ComplexNum &cn);
	
	ComplexNum &operator-(ComplexNum &cn);
	
	ComplexNum &operator*(ComplexNum &cn);
	
	ComplexNum &operator/(ComplexNum &cn);
	
	ComplexNum &operator+(double n);
	
	ComplexNum &operator-(double n);
	
	ComplexNum &operator*(double n);
	
	ComplexNum &operator/(double n);
	
	friend ComplexNum &operator+(double n, ComplexNum &cn);
	
	friend ComplexNum &operator-(double n, ComplexNum &cn);
	
	friend ComplexNum &operator*(double n, ComplexNum &cn);
	
	friend ComplexNum &operator/(double n, ComplexNum &cn);
	
	double getReal() const;
	
	double getImag() const;
	
	friend ostream &operator<<(ostream &s, ComplexNum &cn);
	
	double abs();
	
	bool operator<(ComplexNum &cn);
	
	bool operator>(ComplexNum &cn);
	
	bool operator==(ComplexNum &cn);
	
	~ComplexNum();
};

ComplexNum &operator*(double n, ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = cn.real * n;
	res->imag = cn.imag * n;
	return *res;
}

ComplexNum &operator/(double n, ComplexNum &cn) {
	ComplexNum *res = new ComplexNum(n);
	*res = *res / cn;
	return *res;
}

ComplexNum &operator+(double n, ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = cn.real + n;
	res->imag = cn.imag;
	return *res;
}

ComplexNum &operator-(double n, ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = cn.real - n;
	res->imag = cn.imag;
	return *res;
}

ostream &operator<<(ostream &s, ComplexNum &cn) {
	s << cn.real << setiosflags(ios::showpos)
	  << cn.imag << "i" << endl << resetiosflags(ios::showpos);
	return s;
}

bool ComplexNum::operator==(ComplexNum &cn) {
	return this->real == cn.real and this->imag == cn.imag;
}

bool ComplexNum::operator>(ComplexNum &cn) {
	return this->abs() > cn.abs();
}

bool ComplexNum::operator<(ComplexNum &cn) {
	return this->abs() < cn.abs();
}

double ComplexNum::abs() {
	return pow(pow(this->real, 2) + pow(this->imag, 2), 0.5);
}

ComplexNum::ComplexNum(double real, double imag) : real(real), imag(imag) {
	std::cout << "created" << std::endl;
}

ComplexNum::ComplexNum(ComplexNum &cn) : real(cn.real), imag(cn.imag) {}

ComplexNum::ComplexNum() : ComplexNum(0, 0) {}

ComplexNum::ComplexNum(double real) : ComplexNum(real, 0) {}

ComplexNum &ComplexNum::operator+(ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real + cn.real;
	res->imag = this->imag + cn.imag;
	return *res;
}

ComplexNum &ComplexNum::operator-(ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real - cn.real;
	res->imag = this->imag - cn.imag;
	return *res;
}

ComplexNum &ComplexNum::operator+(double n) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real + n;
	res->imag = this->imag;
	return *res;
}

ComplexNum &ComplexNum::operator-(double n) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real - n;
	res->imag = this->imag;
	return *res;
}

ComplexNum &ComplexNum::operator=(ComplexNum &cn) {
	this->real = cn.real;
	this->imag = cn.imag;
	return *this;
}

ComplexNum &ComplexNum::operator*(ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real * cn.real - this->imag * cn.imag;
	res->imag = this->real * cn.imag - this->imag * cn.real;
	return *res;
}

ComplexNum &ComplexNum::operator/(ComplexNum &cn) {
	ComplexNum *res = new ComplexNum;
	res->real = (this->real * cn.real + this->imag * cn.imag) / (cn.real * cn.real + cn.imag * cn.imag);
	res->imag = (this->imag * cn.real - this->real * cn.imag) / (cn.real * cn.real + cn.imag * cn.imag);
	return *res;
}

ComplexNum &ComplexNum::operator*(double n) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real * n;
	res->imag = this->imag * n;
	return *res;
}

double ComplexNum::getReal() const {
	return real;
}

ComplexNum &ComplexNum::operator/(double n) {
	ComplexNum *res = new ComplexNum;
	res->real = this->real / n;
	res->imag = this->imag / n;
	return *res;
}

double ComplexNum::getImag() const {
	return imag;
}

ComplexNum::~ComplexNum() {
	std::cout << "destroyed" << std::endl;
}


#endif //COMPLEXNUM_COMPLEXNUM_HPP

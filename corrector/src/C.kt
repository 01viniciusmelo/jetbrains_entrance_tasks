fun getPairByNK(N: Long, K: Long): Pair<Long, Long> {
    var prev = K
    var rep = prev
    var q = 1L
    while (rep != 0L) {
        prev = rep
        rep -= (rep and q)
        q = q.shl(1)
    }
    val num = (N - K) / prev
    val right = num / 2
    val left = num - right
    return Pair(left, right)
}

fun main(args: Array<String>) {
    val t = readLine()!!.toInt()
    for (x in 1..t) {
        val (n, k) = readLine()!!.split(" ").map(String::toLong)
        val pair = getPairByNK(n, k)
        println("Case #$x: ${pair.first} ${pair.second}")
    }
}
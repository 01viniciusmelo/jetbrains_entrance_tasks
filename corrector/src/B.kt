import java.lang.Math.pow

fun isTidy(n: Long): Int? {
    val s = n.toString()
    var last: Char = '0'
    var c: Char
    for (i in 0..s.length - 1) {
        c = s[i]
        if (last > c)
            return s.length - i
        last = c
    }
    return null
}

fun getTidyByN(n: Long): Long {
    var n = n
    var res = isTidy(n)
    var pow: Long
    while (res != null) {
        pow = pow(10.0, res.toDouble()).toLong()
        n = n / pow * pow - 1
        res = isTidy(n)
    }
    return n
}

fun main(args: Array<String>) {
    val t = readLine()!!.toInt()
    for (x in 1..t) {
        print("Case #${x}: ${getTidyByN(readLine()!!.toLong())}\n")
    }
}